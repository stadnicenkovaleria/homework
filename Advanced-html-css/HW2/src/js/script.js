const burgerBtn = document.querySelector('.burger-menu')
const menu = document.querySelector('.burger-list')

burgerBtn.addEventListener('click', () => {
  burgerBtn.classList.toggle('menu-open')
  menu.classList.toggle('hide')
})

