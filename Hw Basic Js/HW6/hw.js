//-- Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) 
//і доповніть її наступним функціоналом:

//1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) 
//і зберегти її в полі `birthday`.

//2. Створити метод `getAge()` який повертатиме скільки користувачеві років.

//3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому регістрі, 
//з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.

//- Вивести в консоль результат роботи функції `createNewUser()`, а також функцій `getAge()` та `getPassword()` створеного об'єкта.

function createNewUser(
    userFirstName = prompt("Whats your name?"),
    userSurname = prompt("whats your surname?")  
) {
    newUser = {};
    newUser.firstName = userFirstName;
    newUser.lastName = userSurname;
    function getLogin() {
        return `${(userFirstName.charAt(0) + userSurname).toLowerCase()} `
    }
    console.log(getLogin());
    birthday = prompt("Enter your date of birth","dd.mm.yyyy")
    function getAge() {
      return  `${new Date ().getFullYear() - birthday.slice(6, 10)}`
    }
    function getPassword() {
        return `${(userFirstName.charAt(0).toUpperCase() + userSurname.toLowerCase() + birthday.slice(6, 10))}`
    }
    console.log(getAge());
    console.log(getPassword());
}

 createNewUser();
    
    