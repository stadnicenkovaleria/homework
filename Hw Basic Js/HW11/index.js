/*- У файлі `index.html` лежить розмітка двох полів вводу пароля.
--1. Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, 
іконка змінює свій зовнішній вигляд.
 У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
-- Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
-- Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
2.Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
3.Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – `You are welcome`;
4. Якщо значення не збігаються - вивести під другим полем текст червоного кольору `Потрібно ввести однакові значення`
5. Після натискання на кнопку сторінка не повинна перезавантажуватись
- Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.*/

const icons = document.querySelectorAll(".fas");
icons.forEach((element) => {
  element.addEventListener("click", function () {
    element.classList.toggle("fa-eye-slash");
    const input = element.previousElementSibling;
    if (input.getAttribute("type") === "password") {
      input.setAttribute("type", "text");
    } else {
      input.setAttribute("type", "password");
    }
  });
});

const form = document.querySelector(".password-form");
const error = document.querySelector(".error");
form.addEventListener("submit", (event) => {
  event.preventDefault();
  console.log(form.password);
  if (form.password.value === form.confirmPassword.value) {
    error.innerHTML = "";
    alert("You are welcome");
  } else {
    error.innerHTML = "Потрібно ввести однакові значення";
  }
});
