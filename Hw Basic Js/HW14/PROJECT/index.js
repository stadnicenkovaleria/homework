// - Взяти будь-яке готове домашнє завдання з HTML/CSS.
// - Додати на макеті кнопку "Змінити тему".
// - При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд.
//  При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// - Вибрана тема повинна зберігатися після перезавантаження сторінки
const changeThemeBtn = document.querySelector('.change-theme-btn')

changeThemeBtn.addEventListener('click',() => {
    if (localStorage.getItem('theme') === 'dark') {
        localStorage.removeItem('theme');
    } else {
        localStorage.setItem('theme', 'dark')
    }
    changeTheme ()
});

function changeTheme () {
    if (localStorage.getItem('theme') === 'dark') {
      document.querySelector('html').classList.add('dark')
    } else {
        document.querySelector('html').classList.remove('dark')
    }
};
changeTheme () 