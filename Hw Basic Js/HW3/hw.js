//-- Отримати за допомогою модального вікна браузера число, яке введе користувач.
//- Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа. 
//Якщо таких чисел немає - вивести в консоль фразу "Sorry, no numbers"
//- Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.

const askUserNumber = +prompt("Enter number")
if (askUserNumber < 5) {
   console.log("Sorry, no numbers")
} else {
   for (let i = 0; i <= askUserNumber; i++) {
      if (i % 5 == 0) {
         console.log(i)
      }
   }
}