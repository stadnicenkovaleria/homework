//1) --Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraph = document.getElementsByTagName("p")
for (const p of paragraph) {
    p.style.backgroundColor = "#ff0000"
}
console.log(paragraph)
 
//2) --Знайти елемент із id="optionsList". --Вивести у консоль. -- 
const elementById = document.getElementById("optionsList")
console.log(elementById)

  //Знайти батьківський елемент та вивести в консоль.
const parentElementById = elementById.closest("div");
console.log(parentElementById)

  //--Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const child = parentElementById.childNodes; // использовать parent.Node
console.log(child)

//3)-- Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/> // нету класса, есть id testParagraph
const element = document.getElementById("testParagraph").innerHtml = "This is a paragraph"
console.log(element )

//4) --Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const elementli = document.querySelectorAll("li", "main-header")
elementli.forEach(li => li.className = "nav-item")
console.log(elementli)

//5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.  
const elementByClass = document.getElementsByClassName("section-title")
for (const element of elementByClass) {
    element.classList.remove("section-title")
}
console.log(elementByClass)