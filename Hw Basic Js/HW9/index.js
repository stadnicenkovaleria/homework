//- Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, 
//до якого буде прикріплений список (по дефолту має бути document.body.
    //- кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
    
    //Приклади масивів, які можна виводити на екран:
    
    /*```javascript
    ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
    ```
    
    ```javascript
    ["1", "2", "3", "sea", "user", 23];
    ```
    */
    //- Можна взяти будь-який інший масив.


    //1. Create array 
    const arr =  ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
    //2. create fuction 
    function listArr(arr, parent = document.body) {
    //3. add list(ul) to argument parent
    parent = document.createElement("ul")
     //4. add parent to page
    document.body.prepend(parent)
    //5. sort out array 
    arr.forEach(function (element)  {
    //6. add to each element "li"
    const elementLi= document.createElement("li")
    //7. add content from array's element
    elementLi.innerHTML = element
     //7. add to page
    parent.append(elementLi)
    })
    } 
    listArr(arr, parent)
    
 
  
   
   
   
   
   
   
   
