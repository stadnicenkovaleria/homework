// -- У папці `banners` лежить HTML код та папка з картинками.
// - При запуску програми на екрані має відображатись перша картинка.
// - Через 3 секунди замість неї має бути показано друга картинка.
// - Ще через 3 секунди – третя.
// - Ще через 3 секунди – четверта.
// - Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// - Після запуску програми десь на екрані має з'явитись кнопка з написом `Припинити`.
// - Після натискання на кнопку `Припинити` цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// - Поруч із кнопкою `Припинити` має бути кнопка `Відновити показ`, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
const imges = document.querySelectorAll(".image-to-show");
const btnShow = document.querySelector(".show-btn");
const btnStop = document.querySelector(".stop-btn");
let i = 0;

const slideShow = () => {
  imges.forEach((element) => {
    element.classList.replace("show", "hide");
  });
  const idImg = imges[i];
  idImg.classList.replace("hide", "show");
  i++;
  if (i > imges.length - 1) {
    i = 0;
  }
};
let timeInterval = setInterval(slideShow, 3000);

btnStop.addEventListener("click", () => {
  clearInterval(slideShow)});

btnShow.addEventListener("click", () => {
  setInterval(slideShow, 3000);
});


// slider
window.addEventListener("DOMContentLoaded", () => {
  const slider = () => {
    const slider = document.querySelector(".slider");
    const sliderContentItems = slider.querySelectorAll(".slider-content-item");
    const sliderBreadcrumbsItems = slider.querySelectorAll(".slider-breadcrumbs-item");
    const leftBtn = slider.querySelector(".slider-buttons-btn--left");
    const rightBtn = slider.querySelector(".slider-buttons-btn--right");
    let activeSlider = 0;

    sliderBreadcrumbsItems.forEach((breadcrumb) => {
      breadcrumb.addEventListener("click", (e) => {
        const dataAtr = e.target.closest("li").getAttribute("data-slider");
        sliderContentItems.forEach((slider, i) => {
          if (slider.getAttribute("data-slider") === dataAtr) {
            activeSlider = i;
            slider.classList.add("show");
          } else {
            slider.classList.remove("show");
          }
        });
      });
    });

    leftBtn.addEventListener("click", () => {
      if (activeSlider === 0) {
        activeSlider = sliderContentItems.length - 1;
      } else {
        activeSlider--;
      }
      sliderContentItems.forEach((slider, i) => {
        slider.classList.remove("show");
      });
      sliderContentItems[activeSlider].classList.add("show");
    });

    rightBtn.addEventListener("click", () => {
      if (activeSlider === sliderContentItems.length - 1) {
        activeSlider = 0;
      } else {
        activeSlider++;
      }
      sliderContentItems.forEach((slider, i) => {
        slider.classList.remove("show");
      });
      sliderContentItems[activeSlider].classList.add("show");
    });
  };

  slider();
});
