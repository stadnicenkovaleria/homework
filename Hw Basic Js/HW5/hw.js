//-- Написати функцію `createNewUser()`, яка буде створювати та повертати об'єкт `newUser`.

//-- При виклику функція повинна запитати ім'я та прізвище.

//-- Використовуючи дані, введені юзером, створити об'єкт `newUser` з властивостями `firstName` та `lastName`.

//- Додати в об'єкт `newUser` метод `getLogin()`, який повертатиме першу літеру імені юзера, з'єднану з прізвищем, 
//все в нижньому регістрі (наприклад, `Ivan Kravchenko → ikravchenko`).

//- Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію `getLogin()`.
//Вивести у консоль результат виконання функції.**/
function createNewUser(
    userFirstName = prompt("Whats your name?"),
    userSurname = prompt("whats your surname?")  
) {
    newUser = {};
    newUser.firstName = userFirstName;
    newUser.lastName = userSurname;
    function getLogin() {
        return `${(userFirstName.charAt(0) + userSurname).toLowerCase()} `
    }
    console.log(getLogin());
}

createNewUser();




