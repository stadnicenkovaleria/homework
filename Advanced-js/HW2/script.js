
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const rootDiv = document.querySelector("#root");
const list = document.createElement("ul");

for (let book of books) {
  try {
    if (!book.author) {
      throw new Error(`Error: one properties are missing: author=${book.author} ` );
    } else if (!book.name) {
      throw new Error(`Error: one properties are missing: name=${book.name} `);
    } else if (!book.price) {
      throw new Error( `Error: one properties are missing: price=${book.price} `
      );
    }
    const item = document.createElement("li");
    item.innerHTML = `AUTHOR: ${book.author}, NAME: ${book.name}, PRISE: ${book.price} UAH`;
    list.appendChild(item);
  } catch (error) {
    console.error(error);
  }
}
rootDiv.appendChild(list);
