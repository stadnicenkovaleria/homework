// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// якщо об'єкт А є прототипом об'єкта Б, то об'єкт А успадкує все що є в об'єкті Б, наприклад колір.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// щоб наслідувати все що є в батьківскьому об'є'кті, конструкторі


class Employee {
constructor(name,age,salary) {
this._name = name,
this._age = age,
this._salary = salary
}
get name() {
    return this._name;
}
get age() {
    return this._age;
}
get salary() {
    return this._salary;
}

set name(value) {
    this._name = value;
}
set age(value) {
    this._age = value;
}
set salary(value) {
    this._salary = value;
}
}


class Programmer extends Employee {
    constructor(name,age,salary,lang) {
        super(name,age,salary,);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3
    }
}
let programmer1 = new Programmer ("Oleg", 45, 7000, 'Java')
let programmer2 = new Programmer ("Ihor", 20, 2000, false)
let programmer3 = new Programmer ("Olena", 25, 3500, "Java Script","Java")

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);