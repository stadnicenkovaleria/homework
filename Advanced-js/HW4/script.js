let API = "https://ajax.test-danit.com/api/swapi/films";
const filmDiv = document.getElementById("film");
const list = document.createElement("ul");
filmDiv.append(list);

function getResponce(url) {
  sendRequest = fetch(url)
    .then((responce) => responce.json())
    .then((data) => {
      data.forEach((element) => {
        const item = document.createElement("li");
        list.append(item);
        item.textContent = `NAME ${element.name} NUMBER ${element.episodeId}`;

        const listCharacter = document.createElement("ul");

        item.append(listCharacter);

        element.characters.forEach((elem) => {
          sendRequest = fetch(elem)
            .then((responce) => responce.json())
            .then((data) => {
              const itemCharacter = document.createElement("li");
              listCharacter.append(itemCharacter);
              itemCharacter.textContent = `${data.name}`;
            });
        });

        const desc = document.createElement("p");
        item.append(desc);
        desc.textContent = `${element.openingCrawl}`;
      });
    });
}
getResponce(API);
