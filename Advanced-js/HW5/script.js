// #### Технічні вимоги:

//  - При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій.
//  Для цього потрібно надіслати GET запит на наступні дві адреси:
//    - `https://ajax.test-danit.com/api/json/users`
//    - `https://ajax.test-danit.com/api/json/posts`
//  - Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//  - Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст,
//   а також ім'я, прізвище та імейл користувача, який її розмістив.
//  - На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
//  При натисканні на неї необхідно надіслати DELETE запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`.
//  Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
//  - Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти
//  [тут](https://ajax.test-danit.com/api-pages/jsonplaceholder.html).
//  - Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер,
//  не будуть там збережені. Це нормально, все так і має працювати.
//  - Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас `Card`.
//  При необхідності ви можете додавати також інші класи.

const API = "https://ajax.test-danit.com/api/json/";

async function sendRequest(url, method = 'GET', options) {
  const response = await fetch(url, {
    method: method,
    ...options,
  })
  if (response.ok) {
    if (method !== "DELETE") {
      const result = await response.json();
      return result;
    } else {
      return response;
    }
  } else {
    return new Error("Error");
  }
}

class Card {
  constructor({ name, username, email, title, body, id }) {
    this.name = name;
    this.username = username;
    this.email = email;
    this.title = title;
    this.body = body;
    this.postId = id;
  }

  createPost() {
    const post = document.createElement("div");
    post.classList.add("post");
    post.insertAdjacentHTML(
      "beforeend",
      `
            <div id= ${this.postId}>
          <div class="user">
                <h3 class="name">${this.name}</h3>
                <p class="username">${this.username}</p>
                <p class="email">${this.email}</p>
            </div>
            <p class="title-post">${this.title}</p>
                <h3 class="post-text">${this.body}</h3>
                <button class="delete-btn">DELETE</button>
                </div>
                `
    );
    const btnDelete = post.querySelector(".delete-btn");
    const deletePost = (postId) => {
      sendRequest(`${API}post/${postId}`, "DELETE")
    };
    btnDelete.addEventListener("click",  () => {
      const postId = this.postId;
       deletePost(postId);
      post.remove();
    });
    document.body.appendChild(post);
  }
}

async function getUser(url) {
  const users = await sendRequest(`${API}users`);
  console.log(users);

  users.forEach(async (element) => {
    const postsArr = await sendRequest(`${API}users/${element.id}/posts`); 
    console.log(postsArr);

    postsArr.forEach((elem) => {
      const newUser = new Card({ ...element, ...elem });
      newUser.createPost(); 
    });
  });
}

getUser();

