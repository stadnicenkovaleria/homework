
const API = `https://api.ipify.org/?format=json`
const ipAPI = 'http://ip-api.com/json/'
const findBtn = document.querySelector(".findIp") //ip-btn
const Info = document.querySelector(".ip-info") // add result from request


    async function sendRequest(url, method = 'GET', options) {
        const response = await fetch(url, {
          method: method,
          ...options,
        })
        if (response.ok) {
              const result = response.json()
              return result
            } 
          else {
            return new Error('Error')
          }
    }

     async function findIP () {
        const response = await sendRequest(API)
        console.log(response);
        const ipAdress = response.ip
     }
     async function findUser () {
        const ipInfo = await sendRequest (ipAPI)
        console.log(ipInfo);
        const {   countryCode, country, city, regionName, region} = ipInfo
      
        Info.insertAdjacentHTML(  
            'beforeend',
        ` <p class="info">Country Code: ${countryCode}</p>
        <p class="info">Country: ${country}</p>
        <p class="info">Region: ${regionName}</p>
        <p class="info">City: ${city}</p>
        <p class="info">District: ${region}</p>
        
        `)
   
     }
    findBtn.addEventListener("click", async () => {
        findIP ()
        findUser ()
    
    })
    
