import React from 'react'
import './modal.scss'

export default class Modal extends React.Component {


    render() {
       const {header,text,actions,span,clickOnSpan, color, backgroundColor,handleOutsideClick} = this.props 
   
       return (
        <div className = 'modal-container ' onClick={handleOutsideClick}>
          <div className = 'modal'>
          {span && <button style = {{backgroundColor: backgroundColor}} className = 'modal-close-btn' onClick = {clickOnSpan}>X</button> } 
          <div className = 'modal-text'>
            <h2 style = {{color: color}} className = 'modal-header-text'>{header}</h2>
          <div>{text}</div>
          </div>
          <div className='modal-btn'>{actions}</div>
          </div>
          </div>
       )
    }
   }