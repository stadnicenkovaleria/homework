import React from 'react'
import './button.scss'

export default class Button extends React.Component {
 render() {
    const {text, backgroundColor, onClick, id} = this.props 

    return (
     <button className='btn btn-open' style = {{backgroundColor: backgroundColor}} onClick = {onClick} id = {id}>{text}</button>
    )
 }
}