import React from 'react';
import Button from './components/Button/index.js'
import Modal from './components/Modal/index.js'
import './App.css';

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isModal: 'close',
    };
  }

  closeModal = () => {
    this.setState({ isModal: 'close' });
  };

  handleModal1 = () => {
    this.setState({ isModal: 'one' });
  };

  handleModal2 = () => {
    this.setState({ isModal: 'two' });
  };

  render() {
    const { isModal } = this.state;

    return (
      <>
        <div className="btn-wrapper">
          <Button 
            text={'Open first modal'}
            onClick={this.handleModal1}
            backgroundColor={'rgb(68, 198, 224)'}
          />
          <Button
            text={'Open second modal'}
            onClick={this.handleModal2}
            backgroundColor={'rgb(6, 99, 120)'}
          />
        </div>
      {isModal === 'one' && (
        <Modal color ={'rgb(68, 198, 224)'} header ={'Hello'} text={'It is first modal'} backgroundColor={'rgb(6, 99, 120)'}span clickOnSpan = {this.closeModal} handleOutsideClick= {this.closeModal} actions = {<>
          <Button text={'Ok'} backgroundColor={'rgb(68, 198, 224)'}  onClick = {this.closeModal}/>
          <Button text={'Cancel'} backgroundColor={'rgb(6, 99, 120) '} />
         </>}/>
      )}
      {isModal === 'two' && (
         <Modal color ={'rgb(6, 99, 120)'} header ={'Hey'} text={'It is second modal'} span clickOnSpan = {this.closeModal} handleOutsideClick= {this.closeModal} actions = {<>
          <Button text={'Ok'} backgroundColor={'rgb(6, 99, 120, 0.7) '} />
          <Button text={'Cancel'} backgroundColor={'rgb(68, 198, 224, 0.7)'} />
         </>}/>
      )}
      
      </>
    )
  }
}
