import React, { useState} from 'react';
import Header from "./components/Header/index.js";
import "./App.css";
import AppRoutes from './routes'
import { Outlet } from 'react-router-dom';

const App = () => {

  const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem("favorite")) || [])
  const [bag, setBag] = useState( JSON.parse(localStorage.getItem("bag")) || [])
  const [carrent, setCarrent] = useState({})
  const [products, setProducts] = useState([]) 
  const [isModal, setIsModal] = useState("close")
  const [countBag, setCountBag] = useState(0)


  const closeModal = () => {
    setIsModal("close" );
  };

  const handleModal = () => {
    setIsModal("open" );
  };

  const handleCarrent = (carrent) => {
    setCarrent(carrent)
  }


 const  addFavorite = (id) => {
  setFavorite((prevState) => !prevState)
    const arrayFavorite = localStorage.getItem("favorite");
    let arrayParse;

    if (arrayFavorite) {
      try {
        arrayParse = JSON.parse(arrayFavorite);
        if (!Array.isArray(arrayParse)) {
          arrayParse = [];
        }
      } catch (error) {
        arrayParse = [];
      }
    } else {
      arrayParse = [];
    }

    const indexFavorite = arrayParse.indexOf(id);
    if (indexFavorite === -1) {
      arrayParse.push(id);
    } else {
      arrayParse.splice(indexFavorite, 1);
    }

    localStorage.setItem("favorite", JSON.stringify(arrayParse));
    setFavorite(arrayParse);
  };
    return (
      <>
        <Header
          countBag={bag.length}
          countFav={favorite.length}
        />
        <main>
         <AppRoutes 
              isFavorite={favorite}
              isBag={bag}
              carrent={carrent}
              products={products}
              isModal={isModal}
              countBag={countBag}

              addFavorite={addFavorite}
              handleCarrent={handleCarrent}
              handleModal={handleModal}
              closeModal={closeModal}
         />
         
         <Outlet/>
        </main>
      </>
    );
  }

export default App
