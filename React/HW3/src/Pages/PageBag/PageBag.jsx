import { useState } from "react";
import "./PageBag.scss";
import CardList from "../../components/Product/CardList/CardList";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";


export const PageBag = ({products}) => {
    const [bag, setBag] = useState( JSON.parse(localStorage.getItem("bag")) || products)
    const [isModal, setIsModal] = useState("close");
    const [carrent, setCarrent] = useState({})


      const closeModal = () => {
        setIsModal("close" );
      };
      const handleCarrent = (carrent) => {
        setCarrent(carrent)
      }
      const handleModal = () => {
        setIsModal("open" );
      }
  

    const deleteBag = (id) => {
      const updatedBagList = products.filter((product) => product.id !== id);
      setBag(updatedBagList);
  localStorage.setItem("bag", JSON.stringify(updatedBagList));
  closeModal()
    }

    return (
      <>
        <div className="page-bag__wrapper">
          <h2>Bag List</h2>
          {bag.length > 0 ? (
          bag.map((product) => (
            <CardList
              key={product.id}
              id={product.id}
              img={product.img}
              name={product.name}
              appointment={product.appointment}
              price={product.price}
              handleModal={() => {
                handleModal();
                handleCarrent(product);
              }}
              hideDeletbtn={true}
              hideFavBtn={true}
              hideAddBtn={true}
              deleteBag={deleteBag}
            />
          ))
        ) : (
          <h3 className="page-bag__title">No products in the bag</h3>
        )}
          {isModal === "open" && (
            <Modal
              color={"#000"}
              header={"Are you sure, you want to delete cart?"}
              text={"Accept?"}
              backgroundColor={"#ffff"}
              span
              clickOnSpan={closeModal}
              actions={
                <>
                  <Button
                    text={"Ok"}
                    backgroundColor={"#ffff"}
                    onClick={() => deleteBag(carrent.id)}
                    className="btn-ok"
                  />
                  <Button
                    text={"Cancel"}
                    backgroundColor={"#000"}
                    onClick={closeModal}
                    className="btn-ok btn-cancle"
                  />
                </>
              }
            />
          )}
        </div>
      </>
    );
  };
  export default PageBag;
  