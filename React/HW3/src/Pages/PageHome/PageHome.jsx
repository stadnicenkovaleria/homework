import { useState,useEffect } from 'react';
import sendRequest from "../../helpers/API.js";
import CardList from "../../components/Product/CardList/CardList";
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";

const PageHome = ({ addFavorite}) => {
    const [products, setProducts] = useState([])
    const [isModal, setIsModal] = useState("close")
    const [carrent, setCarrent] = useState({})
    const [bag, setBag] = useState( JSON.parse(localStorage.getItem("bag")) || [])

    const closeModal = () => {
      setIsModal("close" );
    };
  
    const handleModal = () => {
      setIsModal("open" );
    
    };
  
    const handleCarrent = (carrent) => {
      setCarrent(carrent)
    }
  
    useEffect(() => {
        sendRequest("http://localhost:3000/productShop.json").then((data) => {
          setProducts(data);
        });
       },[])


       const  addBag = () => {
        const card = JSON.parse(localStorage.getItem("bag"));
        if (card) {
          localStorage.setItem("bag", JSON.stringify([...card, carrent]));
        } else {
          localStorage.setItem("bag", JSON.stringify([carrent]));
        }
        setBag(JSON.parse(localStorage.getItem("bag")));
       closeModal();
      };
    
 return (
    <>
    {products.map((product) => (
        <CardList
        products={products}
          key={product.id}
          id={product.id}
          img={product.img}
          name={product.name}
          appointment={product.appointment}
          price={product.price}
          handleModal={() => {
            handleModal();
             handleCarrent(product)
          }}
          addFavorite={() => addFavorite(product.id)}
        addBag={addBag} 
        />
      ))}
      {isModal === "open" && (
        <Modal
          color={"#000"}
          header={"Are you sure, you want to add to cart?"}
          text={"Accept"}
          backgroundColor={"#ffff"}
          span
          clickOnSpan={closeModal}
          actions={
            <>
              <Button
                text={"Ok"}
                backgroundColor={"#ffff"}
                onClick={() => {
                 addBag();
                 closeModal()
                }}
                className="btn-ok"
              />
              <Button
                text={"Cancel"}
                backgroundColor={"#000"}
                onClick={closeModal}
                className="btn-ok btn-cancle"
              />
            </>
          }
        />
      )}
      </>
 )
}


export default PageHome
