import { useState, useEffect } from "react";
import "./PageFav.scss";
import sendRequest from "../../helpers/API.js";
import CardList from "../../components/Product/CardList/CardList";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";

export const PageFav = ({ }) => {
  const [products, setProducts] = useState([]);
  const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem("favorite")) || [])
  const [bag, setBag] = useState(JSON.parse(localStorage.getItem("bag")) || [])
  const [isModal, setIsModal] = useState("close");
  const [carrent, setCarrent] = useState({})

  const closeModal = () => {
    setIsModal("close");
  };

  const handleModal = () => {
    setIsModal("open");

  };

  const handleCarrent = (carrent) => {
    setCarrent(carrent)
  }

  useEffect(() => {
    sendRequest("http://localhost:3000/productShop.json").then((data) => {
      setProducts(data);

      const arrayFavorite = localStorage.getItem("favorite");
      if (arrayFavorite) {
        const arrayParse = JSON.parse(arrayFavorite);

        const favoriteProducts = data.filter((product) =>
          arrayParse.includes(product.id)
        );
        setProducts(favoriteProducts);
      }
    });
  }, []);




  const addFavorite = (id) => {
    const updatedProducts = products.filter((product) => product.id !== id);
    setProducts(updatedProducts);

    const arrayFavorite = favorite || [];
    const indexFavorite = arrayFavorite.indexOf(id);
    if (indexFavorite === -1) {
      arrayFavorite.push(id);
    } else {
      arrayFavorite.splice(indexFavorite, 1);
    }
    localStorage.setItem("favorite", JSON.stringify(arrayFavorite));
    setFavorite(arrayFavorite);
  };

  const addToBagFavList = () => {
    const card = JSON.parse(localStorage.getItem("bag"));
    if (card) {
      localStorage.setItem("bag", JSON.stringify([...card, carrent]));
    } else {
      localStorage.setItem("bag", JSON.stringify([carrent]));
    }

    setBag(JSON.parse(localStorage.getItem("bag")));
    const arrayFavorite = JSON.parse(localStorage.getItem("favorite")) || []
    const indexFavorite = arrayFavorite.indexOf(carrent.id);
    if (indexFavorite !== -1) {
      arrayFavorite.splice(indexFavorite, 1);
      localStorage.setItem("favorite", JSON.stringify(arrayFavorite));
      setFavorite(...arrayFavorite);
    }
    closeModal();
  };

  return (
    <>
      <div className="page-fav__wrapper">
        <h2 >Favorites</h2>
        {products.length > 0 ? (
            products.map((product) => (
              <CardList
                products={products}
                key={product.id}
                id={product.id}
                img={product.img}
                name={product.name}
                appointment={product.appointment}
                price={product.price}
                handleModal={() => {
                  handleModal();
                  handleCarrent(product);
                }}
                addToBagFavList={addToBagFavList}
                addFavorite={() => addFavorite(product.id)}
              />
            ))
        ) : (
          <h3 className="page-fav__title">No products in the favorite</h3>
        )}

        {isModal === "open" && (
          <Modal
            color={"#000"}
            header={"Are you sure, you want to add to cart?"}
            text={"Accept"}
            backgroundColor={"#ffff"}
            span
            clickOnSpan={closeModal}
            actions={
              <>
                <Button
                  text={"Ok"}
                  backgroundColor={"#ffff"}
                  onClick={() => {
                    addToBagFavList()
                  }}
                  className="btn-ok"
                />
                <Button
                  text={"Cancel"}
                  backgroundColor={"#000"}
                  onClick={closeModal}
                  className="btn-ok btn-cancle"
                />
              </>
            }
          />
        )}
      </div>
    </>
  );
};
export default PageFav;
