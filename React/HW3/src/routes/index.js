import { Route, Routes } from "react-router-dom";
import PageFav from "../Pages/PageFav/PageFav";
import PageBag from "../Pages/PageBag/PageBag";
import PageHome from "../Pages/PageHome/PageHome";
import Page404 from "../Pages/Page404/Page404";
const AppRoutes = ({
  products,
  addBag,
  addFavorite,
  isBag,
}) => {
  return (
    <Routes>
      <Route
        path={"/"}
        element={<PageHome  addFavorite={addFavorite} />}
      />
      <Route
        path={"/favourites"}
        element={
          <PageFav
            products={products}
            // addFavorite={addFavorite}
          />
        }
      />
      <Route path={"/bagList"} element={<PageBag products={isBag} />} />
      <Route path={"*"} element={<Page404 />} />
    </Routes>
  );
};
export default AppRoutes;
