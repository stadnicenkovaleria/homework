import PropTypes from "prop-types";
import Card from "../Card/index";
import "./CardList.scss";

const CardList = ({ handleModal,img,name, appointment,price,  addFavorite, id,  openModal, hideAddBtn,hideFavBtn,hideDeletbtn}) => {
    return (
      <div className="wrapper">
        <Card
          id={id}
          handleModal={handleModal}
          openModal={openModal}
          img={img}
          name={name}
          appointment={appointment}
          price={price}
          addFavorite={addFavorite}
          hideAddBtn={hideAddBtn}
          hideFavBtn={hideFavBtn}
          hideDeletbtn={hideDeletbtn}
        />
      </div>
    );
  }

export default CardList
CardList.defaultProps = {
  name: "name",
  appointment: "appointment",
  price: "price",
};

CardList.propTypes = {
  handleModal: PropTypes.func,
  addFavorite: PropTypes.func,
  id: PropTypes.number,
  img: PropTypes.string,
  name: PropTypes.string,
  appointment: PropTypes.string,
  price: PropTypes.string,
};
