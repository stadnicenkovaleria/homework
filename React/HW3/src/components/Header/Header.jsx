import PropTypes from 'prop-types';
import "./Header.scss";
// import cx from "classnames"
import { Link} from 'react-router-dom';
import { ReactComponent as Bag } from "./img/bag.svg";
import { ReactComponent as Love } from "./img/favorite.svg";

const Header =({countBag, countFav}) => {
    return (
      <header className="header">
        <div className="container">
          <div className="logo">
            <Link to='/' className="header__logo">
              Medik8
            </Link></div>
          <div className="header__actions">
            <Link to='/bagList' className="header__bag-list">
              <span className="icon-bag">
                <span className=" count count__bag">{countBag}</span>
                <Bag />
              </span>
            </Link>
            <Link to="/favourites" className="header__favorites-list">
              <span className="icon-favorite" >
                <span className="count count__fav">{countFav}</span>
                <Love />
              </span>
            </Link>
          </div>
        </div>
      </header>
    );
  }
  export default Header

Header.propTypes = {
	countBag: PropTypes.number,
	countFav: PropTypes.number
}

