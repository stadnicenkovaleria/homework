import PropTypes from 'prop-types';
import React from "react";
import "./Header.scss";
import { ReactComponent as Bag } from "./img/bag.svg";
import { ReactComponent as Love } from "./img/favorite.svg";

export default class Header extends React.Component {
  render() {
    const { countBag, countFav} = this.props;
    return (
      <header className="header">
        <div className="container">
          <div className="logo">
            <a href="#" className="header__logo">
              Medik8
            </a></div>
          <div className="header__actions">
            <div className="header__bag-list">
              <span className="icon-bag">
                <span className=" count count__bag">{countBag}</span>
                <Bag />
              </span>
            </div>
            <div className="header__favorites-list">
              <span className="icon-favorite" >
                <span className="count count__fav">{countFav}</span>
                <Love />
              </span>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

Header.propTypes = {
	countBag: PropTypes.number,
	countFav: PropTypes.number
}

