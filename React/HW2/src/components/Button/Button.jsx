import PropTypes from 'prop-types';
import React from 'react'
import './button.scss'

export default class Button extends React.Component {
 render() {
    const {text, backgroundColor, onClick, id, className} = this.props 

    return (
     <button  className={className} style = {{backgroundColor: backgroundColor}} onClick = {onClick} id = {id}>{text}</button>
    )
 }
}


Button.defaultProps = {
	text: 'text',
	backgroundColor: '#ffff'
}

Button.propTypes = {
	text: PropTypes.string,
   backgroundColor: PropTypes.string,
   className: PropTypes.string,
   onClick: PropTypes.func
}


