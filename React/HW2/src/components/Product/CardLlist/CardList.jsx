import PropTypes from "prop-types";

import React from "react";
import Card from "../Card/index";
import "./CardList.scss";

export default class CardList extends React.Component {
  state = {};

  render() {
    const {
      handleModal,
      img,
      name,
      appointment,
      price,
      addFavorite,
      id,
      openModal,
    } = this.props;
    return (
      <div className="wrapper">
        <Card
          id={id}
          handleModal={handleModal}
          openModal={openModal}
          img={img}
          name={name}
          appointment={appointment}
          price={price}
          addFavorite={addFavorite}
        />
      </div>
    );
  }
}

CardList.defaultProps = {
  name: "name",
  appointment: "appointment",
  price: "price",
};

CardList.propTypes = {
  handleModal: PropTypes.func,
  addFavorite: PropTypes.func,
  id: PropTypes.number,
  img: PropTypes.string,
  name: PropTypes.string,
  appointment: PropTypes.string,
  price: PropTypes.string,
};
