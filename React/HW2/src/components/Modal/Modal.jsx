import PropTypes from 'prop-types';
import React from 'react'
import './modal.scss'

export default class Modal extends React.Component {


    render() {
       const {header,text,actions,span,clickOnSpan, color, backgroundColor} = this.props 
   
       return (
        <div className = 'modal-container ' >
          <div className = 'modal'>
          {span && <button style = {{backgroundColor: backgroundColor}} className = 'modal-close-btn' onClick = {clickOnSpan}>X</button> } 
          <div className = 'modal-text'>
            <h2 style = {{color: color}} className = 'modal-header-text'>{header}</h2>
          <div>{text}</div>
          </div>
          <div className='modal-btn'>{actions}</div>
          </div>
          </div>
       )
    }
   }

   Modal.defaultProps = {
      text: 'text',
      header: 'header',
      color: '#000',
      backgroundColor: '#ffff',

   }
   
   Modal.propTypes = {
      header: PropTypes.string,
      text: PropTypes.string,
      actions: PropTypes.func,
      span: PropTypes.string,
      color: PropTypes.string,
      clickOnSpan: PropTypes.func,
      backgroundColor: PropTypes.string
   }
   
   
//    return (
//       <div className="productCard">
//           <img className="img" src={image} alt="" />
//           <h1 className="title">{title}</h1>
//           <p className="price">Цена: {price} $</p>
//           <p className="vendor-code">Артикул: {vendorСode}</p>
//           <p>Цвет: {color}</p>
//           <span
//               className={
//                   this.state.isFaworites ? "star star--active" : "star"
//               }
//               onClick={() => {
//                   this.setState((prev) => ({
//                       isFaworites: !prev.isFaworites,
//                   }));
//                   if (!this.state.isFaworites) {
//                       addFavorite(card);
//                   } else {
//                       removeFavorites(card);
//                   }
//               }}
//           >
//               <Star />
//           </span>
//           <Button
//               onClick={() => {
//                   addReadyToCart(card);
//                   openModal();
//               }}
//               text={"Add to cart"}
//               backgroundColor="#d3c1f4"
//               classNames="button--size"
//           />
//       </div>
//   );
// }