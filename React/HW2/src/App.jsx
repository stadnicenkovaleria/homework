import React from "react";
import Header from "./components/Header/index.js";
import CardList from "./components/Product/CardLlist/index.js";

import Button from "./components/Button/index.js";
import Modal from "./components/Modal/index.js";
import sendRequest from "./helpers/API.js";
import "./App.css";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      isFavorite: JSON.parse(localStorage.getItem("favorite")) || [],
      isBag: JSON.parse(localStorage.getItem("bag")) || [],
      isModal: "close",
      carrent:{},
      countBag: 0,
    };
  }

  closeModal = () => {
    this.setState({ isModal: "close" });
  };

  handleModal = () => {
    this.setState({ isModal: "open" });
  };
  handleCarrent = (carrent) => {
    this.setState({carrent: carrent})
  }

  componentDidMount() {
    sendRequest("http://localhost:3000/productShop.json").then((data) => {
      this.setState({ products: data });
    });
  }

  addFavorite = (id) => {
    const arrayFavorite = localStorage.getItem("favorite");
    let arrayParse;

    if (arrayFavorite) {
      try {
        arrayParse = JSON.parse(arrayFavorite);
        if (!Array.isArray(arrayParse)) {
          arrayParse = [];
        }
      } catch (error) {
        arrayParse = [];
      }
    } else {
      arrayParse = [];
    }

    const indexFavorite = arrayParse.indexOf(id);
    if (indexFavorite === -1) {
      arrayParse.push(id);
    } else {
      arrayParse.splice(indexFavorite, 1);
    }

    localStorage.setItem("favorite", JSON.stringify(arrayParse));
    this.setState({ isFavorite: arrayParse });
  };

  addBag = () => {
    const card = JSON.parse(localStorage.getItem("bag"));
    if (card) {
      localStorage.setItem("bag", JSON.stringify([...card, this.state.carrent]));
    } else {
      localStorage.setItem("bag", JSON.stringify([this.state.carrent]));
    }
    this.setState({ isBag: JSON.parse(localStorage.getItem("bag")) });
    console.log(this.state.carrent );
    this.closeModal();
  };
  render() {
    const { products, addFavorite, isBag, isModal } = this.state;
    return (
      <>
        <Header
          countBag={this.state.isBag.length}
          countFav={this.state.isFavorite.length}
        />
        <main>
          {products.map((product) => (
            <CardList
              key={product.id}
              id={product.id}
              img={product.img}
              name={product.name}
              appointment={product.appointment}
              price={product.price}
              handleModal={() => {
                this.handleModal();
                 this.handleCarrent(product)
              }}
              addFavorite={() => this.addFavorite(product.id)}
              addBag={this.addBag.bind(this, product)}
            />
          ))}
          {isModal === "open" && (
            <Modal
              color={"#000"}
              header={"Are you sure, you want to add to cart?"}
              text={"Accept"}
              backgroundColor={"#ffff"}
              span
              clickOnSpan={this.closeModal}
              actions={
                <>
                  <Button
                    text={"Ok"}
                    backgroundColor={"#ffff"}
                    onClick={() => {
                      this.addBag(products);
                    }}
                    className="btn-ok"
                  />
                  <Button
                    text={"Cancel"}
                    backgroundColor={"#000"}
                    onClick={this.closeModal}
                    className="btn-ok btn-cancle"
                  />
                </>
              }
            />
          )}
        </main>
      </>
    );
  }
}
