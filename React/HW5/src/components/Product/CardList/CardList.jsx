import {useSelector} from "react-redux";
import "./CardList.scss";
import Card from "../Card";

const CardList = ({handleAddBag}) => {
    const dataStore = useSelector((state) => state.data);

    const cards = dataStore.map((item) => (
        <Card
            key={item.id}
            img={item.img}
            name={item.name}
            price={item.price}
            id={item.id}
            poroduct={item}
            handleAddBag={handleAddBag}
        />
    ))

    return (
      <div className="wrapper">
          {
              cards
          }
      </div>
    );
  }

export default CardList
